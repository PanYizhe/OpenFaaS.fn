rm -r ./template/
cp -r ./RISC-V_OpenFaaS.template/ ./template/

function upFunction(){
    echo "UP function $1 !"
    faas-cli up -f ./$1.yml
}

cat ./function/AllFunctions | while read line
do
    upFunction riscv64-$line
done