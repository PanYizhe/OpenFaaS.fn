# 192.168.1.189
function wrkTest(){
    # $1: ip
    # $2: function name
    # $3: architecture
    echo "ip: $1"
    echo "fname: $2"
    echo "arch: $3"
    wrk -t 1 -c 1 -d 10s --latency -s ./function/$2/wrk.lua http://$1:8080/function/$3-$2 > ./function/$2/$3.wrkres
    echo "------- ------- -------"
}

riscv64="192.168.10.2"
x86="10.30.245.114"

cat ./function/functionTest | while IFS=$' \t\r\n' read -r line
do
    echo \### $line
    echo Test for riscv64-$line
    wrkTest $riscv64 $line riscv64
    echo Test for x86-$line
    wrkTest $x86 $line x86
done