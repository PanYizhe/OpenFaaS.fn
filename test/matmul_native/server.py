from flask import Flask, render_template
import main
import time

app = Flask(__name__, template_folder="./FrontEnd")


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/run/')
def get():
    st = time.time()
    res = main.run()
    et = time.time()
    return {
        "res": res,
        "time": (et - st) * 1000
    }


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8888)
