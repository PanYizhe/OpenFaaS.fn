import random
import time


def generate_matrix(rows, cols):
    """
    生成一个随机矩阵
    :param rows: 矩阵的行数
    :param cols: 矩阵的列数
    :return: 随机矩阵
    """
    matrix = []
    for i in range(rows):
        row = []
        for j in range(cols):
            num = random.randint(0, 10)
            row.append(num)
        matrix.append(row)
    return matrix


def matrix_multiplication(matrix1, matrix2):
    """
    用内置库实现矩阵相乘
    :param matrix1: 第一个矩阵，形状为 (m, n)
    :param matrix2: 第二个矩阵，形状为 (n, p)
    :return: 相乘结果矩阵，形状为 (m, p)
    """
    result = []
    for i in range(len(matrix1)):
        row = []
        for j in range(len(matrix2[0])):
            sum = 0
            for k in range(len(matrix1[0])):
                sum += matrix1[i][k] * matrix2[k][j]
            row.append(sum)
        result.append(row)
    return result


def run():
    matrix1 = generate_matrix(100, 100)
    matrix2 = generate_matrix(100, 100)
    start = time.time()
    result = matrix_multiplication(matrix1, matrix2)
    end = time.time()
    res = "Time cost: %f" % ((end - start) * 1000)
    print(res)
    return res


if __name__ == '__main__':
    run()
