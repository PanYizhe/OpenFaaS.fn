let st = Date.now();

const argv = process.argv;

console.log(argv.length)
console.log(argv)


function generateRandomMatrix(n) {
    const matrix = [];
    for (let i = 0; i < n; i++) {
        const row = [];
        for (let j = 0; j < n; j++) {
            row.push(i * j);
        }
        matrix.push(row);
    }
    return matrix;
}

function matrixMultiply(matrix1, matrix2) {
    if (matrix1[0].length !== matrix2.length) {
        return "Error: The number of columns in the first matrix must be equal to the number of rows in the second matrix.";
    }

    const result = [];
    for (let i = 0; i < matrix1.length; i++) {
        result[i] = [];
        for (let j = 0; j < matrix2[0].length; j++) {
            let sum = 0;
            for (let k = 0; k < matrix1[0].length; k++) {
                sum += matrix1[i][k] * matrix2[k][j];
            }
            result[i][j] = sum;
        }
    }

    return result;
}

const n = 10000; // 阶数
const matrix = generateRandomMatrix(n);

let et = Date.now();
const elapsed = et - st;
console.log('Elapsed time:', elapsed, 'milliseconds');