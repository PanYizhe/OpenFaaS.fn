import time


def handle(req):
    st = time.time()
    """handle a request to the function
    Args:
        req (str): request body
    """

    et = time.time()
    return (et - st) * 1000
