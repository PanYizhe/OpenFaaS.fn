import json
from scipy.optimize import curve_fit
import time


def handle(req):
    st = time.time()
    """handle a request to the function
        Args:
            req (str): request body
    """
    event = json.loads(req)
    X = event['X']
    y = event['y']

    def func(x, a, b):
        y = a * x + b
        return y

    try:
        popt, pcov = curve_fit(func, X, y)
    except Exception as e:
        print(e)
        # return e
    else:
        # return popt, pcov
        pass

    et = time.time()
    return (et - st) * 1000


if __name__ == "__main__":
    print(handle('{"X": [150, 200, 250, 350, 300, 400, 600], "y": [6450, 7450, 8450, 9450, 11450, 15450, 18450]}'))
