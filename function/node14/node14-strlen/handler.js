'use strict'

module.exports = async (event, context) => {
    let st = new Date().getTime()
    let str = event.body
    let res = str.length
    let et = new Date().getTime()
    const result = {
        'res': JSON.stringify(res),
        'time': JSON.stringify(et - st),
        'content-type': event.headers["content-type"]
    }
    return context
        .status(200)
        .succeed(result)
}
