import numpy as np
from time import time
import json


def matmul(n):
    A = np.random.rand(n, n)
    B = np.random.rand(n, n)

    start = time()
    C = np.matmul(A, B)
    latency = time() - start
    return latency


def handle(req):
    st = time()
    """handle a request to the function
    Args:
        req (str): request body
    """
    json_req = json.loads(req)
    n = int(json_req['n'])
    result = matmul(n)

    et = time()
    return (et - st) * 1000


if __name__ == "__main__":
    test_data = [
        {"n": 100},
        {"n": 1000},
        {"n": 10000},
    ]
    for data in test_data:
        print(matmul(data['n']))
