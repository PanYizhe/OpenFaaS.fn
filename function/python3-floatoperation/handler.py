import json
import math
from time import time


def float_operation(N):
    start = time()
    for i in range(0, N):
        sin_i = math.sin(i)
        cos_i = math.cos(i)
        sqrt_i = math.sqrt(i)
    latency = time() - start
    return latency


def handle(req):
    st = time()
    """handle a request to the function
    Args:
        req (str): request body
    """
    json_req = json.loads(req)
    n = int(json_req['n'])

    latency = float_operation(n)
    et = time()
    return (et - st) * 1000


if __name__ == "__main__":
    test_req = '{"n": 100000}'
    print(handle(test_req))
