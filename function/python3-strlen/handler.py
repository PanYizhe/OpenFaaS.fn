import time
import random


def handle(req):
    st = time.time()
    """handle a request to the function
    Args:
        req (str): request body
    """

    res = len(req)

    et = time.time()
    return (et - st) * 1000


if __name__ == "__main__":
    def generate_random_string(length):
        letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        result_str = ''.join(random.choice(letters) for i in range(length))
        return result_str


    for i in range(1):
        length = 1000000
        random_string = generate_random_string(length)
        res = handle(random_string)
        print(res)
