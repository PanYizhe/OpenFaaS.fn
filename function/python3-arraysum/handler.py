import time
import json


def sum(lst):
    if len(lst) == 0:
        return 0
    else:
        return lst[0] + sum(lst[1:])


def handle(req):
    st = time.time()
    """handle a request to the function
    Args:
        req (str): request body
    """
    lst=json.loads(req)
    res = sum(lst)

    et = time.time()
    return (et - st) * 1000


if __name__ == "__main__":
    lst = "[1, 2, 3, 4, 5]"
    res = handle(lst)
    print(type(res))
    print(res)
