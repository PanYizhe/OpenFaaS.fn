import time
import nltk

nltk.download('punkt')


def handle(req):
    st = time.time()
    """handle a request to the function
    Args:
        req (str): request body
    """
    sentence = req
    tokens = nltk.word_tokenize(sentence)

    et = time.time()
    return (et - st) * 1000


if __name__ == "__main__":
    res = handle("Hello World")
    print(res)
