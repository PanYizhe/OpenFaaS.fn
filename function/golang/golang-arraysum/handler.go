package function

import (
	"encoding/binary"
	"fmt"
	"time"
)

func convertToIntArray(data []byte) []int {
	var intArr []int
	for i := 0; i < len(data); i += 4 {
		value := binary.BigEndian.Uint32(data[i : i+4])
		intArr = append(intArr, int(value))
	}
	return intArr
}

func sum(arr []int, index int) int {
	if index == len(arr) {
		return 0
	}
	return arr[index] + sum(arr, index+1)
}

// Handle a serverless request
func Handle(req []byte) string {
	startTime := getTime()
	paramList := convertToIntArray(req)
	_ = sum(paramList, 0)
	retTime := getTime()
	return fmt.Sprintf("%f", retTime-startTime)
}

// getTime 获取当前时间戳
func getTime() float64 {
	return float64(time.Now().UnixNano() / 1e6)
}
