package function

import (
	"fmt"
	"math"
	"time"
)

// 最小二乘法
func leastSquares(x, y []float64) (float64, float64, error) {
	// 计算样本点的个数
	n := len(x)
	// 计算 x 的平均值
	xMean := 0.0
	for i := 0; i < n; i++ {
		xMean += x[i]
	}
	xMean /= float64(n)
	// 计算 y 的平均值
	yMean := 0.0
	for i := 0; i < n; i++ {
		yMean += y[i]
	}
	yMean /= float64(n)
	// 计算斜率
	sum := 0.0
	for i := 0; i < n; i++ {
		sum += (x[i] - xMean) * (y[i] - yMean)
	}
	slope := sum / sumOfSquares(x, xMean)
	// 计算截距
	intercept := yMean - slope*xMean
	return slope, intercept, nil
}

// 计算 x 的平均值
func sumOfSquares(x []float64, xMean float64) float64 {
	sum := 0.0
	for i := 0; i < len(x); i++ {
		sum += math.Pow(x[i]-xMean, 2)
	}
	return sum
}

// Handle a serverless request
func Handle(req []byte) string {
	startTime := getTime()
	// 样本点
	x := []float64{1, 2, 3, 4, 5}
	y := []float64{2, 4, 6, 8, 10}
	_, _, _ = leastSquares(x, y)
	retTime := getTime()
	return fmt.Sprintf("%f", retTime-startTime)
}

// getTime 获取当前时间戳
func getTime() float64 {
	return float64(time.Now().UnixNano() / 1e6)
}
