package function

import (
	"fmt"
	"time"
)

// Handle a serverless request
func Handle(req []byte) string {
	startTime := getTime()
	_ = len(req)
	retTime := getTime()
	return fmt.Sprintf("%f", retTime-startTime)
}

// getTime 获取当前时间戳
func getTime() float64 {
	return float64(time.Now().UnixNano() / 1e6)
}
