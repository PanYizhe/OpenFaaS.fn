package function

import (
	"fmt"
	"strconv"
	"time"
)

// Handle a serverless request
func Handle(req []byte) string {
	startTime := getTime()
	s := string(req)
	i, _ := strconv.ParseInt(s, 10, 64)
	_ = i * 2
	retTime := getTime()
	return fmt.Sprintf("%f", retTime-startTime)
}

// getTime 获取当前时间戳
func getTime() float64 {
	return float64(time.Now().UnixNano() / 1e6)
}
