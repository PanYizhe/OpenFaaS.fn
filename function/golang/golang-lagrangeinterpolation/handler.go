package function

import (
	"fmt"
	"time"
)

// Lagrange插值函数
func Lagrange(x []float64, y []float64, t float64) float64 {
	n := len(x)
	res := y[0]
	for i := 0; i < n; i++ {
		term := 1.0
		for j := 0; j < n; j++ {
			if i != j {
				term *= (t - x[j]) / (x[i] - x[j])
			}
		}
		res += term * y[i]
	}
	return res
}

// Handle a serverless request
func Handle(req []byte) string {
	startTime := getTime()
	// 自变量的取值
	x := []float64{1, 3, 5}
	// 对应的函数值
	y := []float64{3, 5, 9}
	// 需要计算的点
	t := 4.0
	_ = Lagrange(x, y, t)
	retTime := getTime()
	return fmt.Sprintf("%f", retTime-startTime)
}

// getTime 获取当前时间戳
func getTime() float64 {
	return float64(time.Now().UnixNano() / 1e6)
}
