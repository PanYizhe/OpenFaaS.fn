package function

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

// 模拟ALU的函数
func doAlu(times int) {
	// 设置随机数种子
	rand.Seed(time.Now().UnixNano())
	// 生成10到100之间的随机整数
	a := rand.Intn(91) + 10
	b := rand.Intn(91) + 10
	for i := 0; i < times; i++ {
		// 计算加减乘除
		switch i % 4 {
		case 0:
			a += b
		case 1:
			a -= b
		case 2:
			a *= b
		case 3:
			a /= b
		}
	}
}

// Handle a serverless request
func Handle(req []byte) string {
	startTime := getTime()
	param := string(req)
	times, _ := strconv.Atoi(param)
	doAlu(times)
	retTime := getTime()
	return fmt.Sprintf("%f", retTime-startTime)
}

// getTime 获取当前时间戳
func getTime() float64 {
	return float64(time.Now().UnixNano() / 1e6)
}
