package function

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"sync"
	"time"
)

// 模拟ALU的函数
func doAlu(times int) {
	// 设置随机数种子
	rand.Seed(time.Now().UnixNano())
	// 生成10到100之间的随机整数
	a := rand.Intn(91) + 10
	b := rand.Intn(91) + 10
	for i := 0; i < times; i++ {
		// 计算加减乘除
		switch i % 4 {
		case 0:
			a += b
		case 1:
			a -= b
		case 2:
			a *= b
		case 3:
			a /= b
		}
	}
}

// Handle a serverless request
func Handle(req []byte) string {
	startTime := getTime()
	params := string(req)
	paramList := strings.Split(params, ",")
	loopTime, _ := strconv.Atoi(paramList[0])
	parallelIndex, _ := strconv.Atoi(paramList[1])
	// 定义一个waitGroup，用来等待所有goroutine执行完毕
	var wg sync.WaitGroup
	// 计算每个进程需要执行的ALU操作次数
	perTimes := int(loopTime / parallelIndex)
	// 执行parallelIndex个goroutine
	for i := 0; i < parallelIndex; i++ {
		// 增加waitGroup计数
		wg.Add(1)
		// 启动goroutine
		//fmt.Printf("Start goroutine %d.\n", i)
		go func() {
			// 执行后减少waitGroup计数
			defer wg.Done()
			// 执行任务
			// TODO: 根据实际需求编写任务逻辑
			doAlu(perTimes)
		}()
	}
	// 等待所有goroutine执行完毕
	wg.Wait()
	retTime := getTime()
	return fmt.Sprintf("%f", retTime-startTime)
}

// getTime 获取当前时间戳
func getTime() float64 {
	return float64(time.Now().UnixNano() / 1e6)
}
