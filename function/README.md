# Function

- functionList: new functions
- AllFunctions: all functions without errors
- functionErrs: functions with error
- functionTest: functions needed testing

ServerlessBench

- https://github.com/SJTU-IPADS/ServerlessBench
- https://github.com/tjulym/gray/blob/main/benchmarks/ServerlessBench

FunctionBench

- https://github.com/ddps-lab/serverless-faas-workbench
- https://github.com/tjulym/gray/tree/main/benchmarks/FunctionBench

DeathStarBench

- https://github.com/delimitrou/DeathStarBench
- https://github.com/tjulym/gray/tree/main/benchmarks/DeathstarBench

TPC-W

- https://www.tpc.org/tpcw
- https://github.com/tjulym/gray/tree/main/benchmarks/TPC-W

EdgeFaaSBench

- https://github.com/kaustubhrajput46/EdgeFaaSBench/tree/main/functions