import time
import random


def alu(times):
    a = random.randint(10, 100)
    b = random.randint(10, 100)
    temp = 0
    for i in range(times):
        if i % 4 == 0:
            temp = a + b
        elif i % 4 == 1:
            temp = a - b
        elif i % 4 == 2:
            temp = a * b
        else:
            temp = a / b
    # print(times)
    return temp


def handle(req):
    st = time.time()
    """handle a request to the function
    Args:
        req (str): request body
    """

    res = alu(int(req))

    et = time.time()
    return (et - st) * 1000


if __name__ == "__main__":
    defaultLoopTime = 10000000
    res = handle(defaultLoopTime)
    print(type(res))
    print(res)
