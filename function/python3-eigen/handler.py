import numpy as np
import json
import time


def handle(req):
    st = time.time()
    event = json.loads(req)
    matrix = event["matrix"]
    eigenvalue, featurevector = np.linalg.eig(matrix)
    # return {
    #     "eigenvalue": eigenvalue,
    #     "featurevector": featurevector
    # }
    et = time.time()
    return (et - st) * 1000


if __name__ == "__main__":
    res = handle('{"matrix": [[1, 0, 0],[0, 1, 0],[0, 0, 1]]}')
    print(res)
