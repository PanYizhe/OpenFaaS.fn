from unittest import result

import numpy as np
import time as t


def handle(req):
    startTime = t.time()
    x = np.random.rand(int(req))
    # print(x)
    for i in range(100000):
        y = np.fft.fft

    endTime = t.time()
    functionTime = endTime - startTime
    return functionTime * 1000


if __name__ == "__main__":
    res = handle("10000")
    print(res)
