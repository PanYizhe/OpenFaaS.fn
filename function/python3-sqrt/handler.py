from math import sqrt, pi
import time as t


def handle(req):
    """This function will calculate the cosine(360) multiple times"""
    startTime = t.time()
    for i in range(1001):
        for x in range(361):
            result = sqrt(x * pi / 180)
    endTime = t.time()
    # elapsedTime = "Total time to execute the function is: " + str(endTime - startTime) + " seconds"
    elapsedTime = endTime - startTime
    return elapsedTime * 1000


if __name__ == "__main__":
    print(handle(None))
