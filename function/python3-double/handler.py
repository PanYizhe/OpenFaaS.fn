import time


def handle(req):
    st = time.time()
    """handle a request to the function
    Args:
        req (str): request body
    """

    num = int(req)
    res = num * 2

    et = time.time()
    return (et - st) * 1000


if __name__ == "__main__":
    res = handle(10)
    print(type(res))
    print(res)
