from gtts import gTTS
import os
import time as t


def handle(req):
    startTime = t.time()

    text = req
    language = "en"
    audio = gTTS(text=text, lang=language, slow=False)
    # 保存音频到本地
    audio.save("audio.wav")
    # 播放音频
    os.system("start audio.wav")

    endTime = t.time()
    return (endTime - startTime) * 1000


if __name__ == "__main__":
    res = handle("Hello World")
    print(res)
