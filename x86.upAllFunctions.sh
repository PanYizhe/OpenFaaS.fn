rm -r ./template/
cp -r ./X86_OpenFaaS.template/ ./template/

function upFunction(){
    echo "UP function $1 !"
    faas-cli up -f ./$1.yml
}

cat ./function/AllFunctions | while read line
do
    upFunction x86-$line
done