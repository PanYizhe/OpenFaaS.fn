#!/bin/bash

function pyz(){
  # faas python3 strlen
  if [ "$1" = "faas" ]; then
    case $2 in
      "python3")
        ;;
      "golang")
        ;;
      "node14")
        ;;
      *)
        echo "unknown lang"
        return
        ;;
    esac
    echo riscv64-$2-$3.yml
    echo x86-$2-$3.yml
    echo \
"version: 1.0
provider:
  name: openfaas
  gateway: http://127.0.0.1:8080
functions:
  riscv64-$2-$3:
    lang: $2
    handler: ./function/$2-$3
    image: 13051172893/riscv64-$2-$3:latest" > riscv64-$2-$3.yml
    echo \
"version: 1.0
provider:
  name: openfaas
  gateway: http://127.0.0.1:8080
functions:
  x86-$2-$3:
    lang: $2
    handler: ./function/$2-$3
    image: 13051172893/x86-$2-$3:latest" > x86-$2-$3.yml
    mkdir ./function/$2-$3
    echo $2-$3 >> ./function/AllFunctions
    echo $2-$3 >> ./function/functionList
  ### ------- ###
  # git deploy
  # git submodule
  elif [ "$1" = "git" ]; then
    case $2 in
      "deploy")
        sh pyz.sh
        ;;
      "submodule")
        echo "git submodule update"
        git submodule update --recursive --remote
        ;;
      "checkout")
        echo "git checkout ."
        git checkout .
        ;;
      *)
        echo "unknown command"
        ;;
    esac
  fi
}

### Main ###
echo $@
pyz $@
while true; do
  echo "### ------- ###"
  read p1 p2 p3
  pyz $p1 $p2 $p3
done
echo $1 $3