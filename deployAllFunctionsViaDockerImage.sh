function deployFunctionViaDockerImage(){
    echo "Deploying $1 !"
    faas-cli deploy --image 13051172893/$1 --name swarm-$1
}

# Python3
deployFunctionViaDockerImage "x86-python3-double"
deployFunctionViaDockerImage "x86-python3-strlen"
#deployFunctionViaDockerImage "x86-python3-eigen"
deployFunctionViaDockerImage "x86-python3-alumultithread"
deployFunctionViaDockerImage "x86-python3-alusequential"
deployFunctionViaDockerImage "x86-python3-arraysum"
deployFunctionViaDockerImage "x86-python3-lagrangeinterpolation"
deployFunctionViaDockerImage "x86-python3-leastsquare"
# Golang
deployFunctionViaDockerImage "x86-golang-alumultithread"
deployFunctionViaDockerImage "x86-golang-alumultithread"
deployFunctionViaDockerImage "x86-golang-alusequential"
deployFunctionViaDockerImage "x86-golang-arraysum"
deployFunctionViaDockerImage "x86-golang-lagrangeinterpolation"
deployFunctionViaDockerImage "x86-golang-leastsquare"