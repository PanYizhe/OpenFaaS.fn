# OpenFaaS.fn

- connection
  - root path: /home/tank/pyz
- mapping
  - 1
    - local path: /
    - deployment path: /OpenFaaS.fn

```sh
git clone https://gitee.com/PanYizhe/OpenFaaS.fn.git --recursive
```

> docker swarm + openfaas

```sh
# x86
./x86.upAllFunctions.sh
# riscv64
./riscv64.upAllFunctions.sh
# x86 faas-swarm
./deployAllFunctionsViaDockerImage.sh
```

- submodule

```sh
# RISC-V_OpenFaaS.template
# X86_OpenFaaS.template
# Chai_OpenFaaS
git submodule update --recursive --remote
```

- Chai_OpenFaaS

```sh
cd Chai_OpenFaaS
screen -S chai-openfaas
pip install -r requirements.txt
python3 Main.py
Ctrl+A+D
```

